Přes rozhraní RS-232 je připojen teploměr od výrobce Papouch. Skriptem jsou na dotaz získána data z teploměru, vyhodnocena a vrácena 0, 1 nebo 2 dle zadaných parametrů.

Thermometer of Papouch vendor is connected via RS-232. This PERL script gain data from thermometer on request. Data are evaluated and 0, 1 or 2 is returned according specified range defined in parameter of the script.
