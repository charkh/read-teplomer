#!/usr/bin/perl -X

# branch "nagios"
 
# argumenty skriptu:
# prvni_cislo ... hodnota stupnu v celých číslech na dosazení limitu warning; ve stupnich C
# druhe_cislo ... hodnota stupnů v celých číslech na dosazení limitu critical; ve stupnich C

# návratový kód skriptu:
# 0 ... vše ok
# 1 ... warning
# 2 ... critical

use Device::SerialPort; 
use Time::gmtime; 


#
# Nastaveni promenných
#
 
$PORT = "/dev/ttyS0";  
$w_cislo = $ARGV[0];
$c_cislo = $ARGV[1];

 
# 
# Nastavení portu 
# 

# Důležitá poznámka vyrobce:
# make the serial port object note the need to convert carriage returns 
# to new lines to terminate each read. 

$ob = Device::SerialPort->new($PORT) || die "Není možné otevřít port $PORT: $!"; 
$ob->baudrate(9600) || die "Selhalo pri nastaveni baudrate"; 
$ob->parity("none") || die "Selhalo pri nastaveni parity"; 
$ob->databits(8) || die "Selhalo pri nastaveni databits"; 
$ob->stty_icrnl(1) || die "Selhalo pri nastaveni konverze z CR na NEW LINE"; 
$ob->handshake("none") || die "Selhalo pri nastaveni handshake"; 
$ob->write_settings || die "zadne nastaveni"; 

# 
# otevření portu 
# 

open( DEV, "<$PORT" ) || die "Neni mozne otevrit port $PORT: $!"; 


#
# načtení a zpracování dat
#

$data = <DEV>;
$data=~s/[C]*$//;
$data = $data+0; 
undef $ob; 


#
# výpis a ukončení dle parametrů
#

if ($data < $w_cislo)
{
#	print "Normal: "
	print $data, "\n";
	exit(0);
}

if ($data > $c_cislo)
{
#	print "Critical: "
	print $data, "\n";
	exit(2);
}

if ($data > $w_cislo)
{
#	print "Warning: "
	print $data, "\n";
	exit(1);
}


